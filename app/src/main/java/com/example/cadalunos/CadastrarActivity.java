package com.example.cadalunos;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CadastrarActivity extends AppCompatActivity {

    private EditText nome;
    private EditText email;
    private EditText telefone;
    private EditText idade;
    private AlunoDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        Button button = findViewById(R.id.buttonCadastrar);

        nome = findViewById(R.id.campoNome);
        email = findViewById(R.id.campoEmail);
        telefone = findViewById(R.id.campoTelefone);
        idade = findViewById(R.id.campoIdade);
        dao = new AlunoDAO(this);


        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String campoNome = nome.getText().toString();
                String campoEmail = email.getText().toString();
                String campoTelefone = telefone.getText().toString();
                String campoIdade = idade.getText().toString();

                Aluno aluno = new Aluno();
                aluno.setNome(campoNome);
                aluno.setEmail(campoEmail);
                aluno.setTelefone(campoTelefone);
                aluno.setIdade(campoIdade);
                long id = dao.inserir(aluno);

                if(id==-1)
                    Toast.makeText(CadastrarActivity.this, "Aluno  Não Cadastrado", Toast.LENGTH_LONG).show();
                else
                Toast.makeText(CadastrarActivity.this, "Aluno Cadastrado com Sucesso", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(CadastrarActivity.this, ListarActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }
}

