package com.example.cadalunos;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenciasUsuario {

    public static final String PREF_ID = "cadalunos";

    public static void setValuesBoolean(Context context, String chave, boolean valor) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_ID, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(chave, valor);
        editor.commit();
    }

    public static boolean getValuesBoolean(Context context, String chave) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_ID, 0);
        boolean b = preferences.getBoolean(chave, false);
        return b;
    }


    public static void setValuesString(Context context, String chave, String valor) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_ID, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(chave, valor);
        editor.commit();
    }

    public static String getValuesString(Context context, String chave) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_ID, 0);
        String b = preferences.getString(chave, "");
        return b;
    }

}
