package com.example.cadalunos;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AtualizarActivity extends AppCompatActivity {

    private Cursor cursor;
    private AlunoDAO dao;
    private String codigo;
    private EditText nome;
    private EditText email;
    private EditText telefone;
    private EditText idade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atualizar);

        codigo = this.getIntent().getStringExtra("cod");

       dao = new AlunoDAO(getBaseContext());

        nome = findViewById(R.id.campoNome);
        email = findViewById(R.id.campoEmail);
        telefone = findViewById(R.id.campoTelefone);
        idade = findViewById(R.id.campoIdade);

        cursor = dao.buscarID(Integer.parseInt(codigo));

        nome.setText(cursor.getString(cursor.getColumnIndexOrThrow("nome")));
        email.setText(cursor.getString(cursor.getColumnIndexOrThrow("email")));
        telefone.setText(cursor.getString(cursor.getColumnIndexOrThrow("telefone")));
        idade.setText(cursor.getString(cursor.getColumnIndexOrThrow("idade")));

        Button alterar = (Button)findViewById(R.id.edit);
        alterar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dao.alterar(Integer.parseInt(codigo), nome.getText().toString(), email.getText().toString(), telefone.getText().toString(), idade.getText().toString());
                Toast.makeText(getApplicationContext(), R.string.msg_alterado, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(AtualizarActivity.this, ListarActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Button deletar = (Button)findViewById(R.id.delet);
        deletar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dao.deletar(Integer.parseInt(codigo));
                Toast.makeText(getApplicationContext(), R.string.msg_excluido , Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(AtualizarActivity.this,ListarActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
