package com.example.cadalunos;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class ListarActivity extends LifeCycle {
    private static final String TAG = "services";

    private ListView listView;
    private AlunoDAO dao;
    private List<Aluno> alunos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        AlunoDAO crud = new AlunoDAO(getBaseContext());
        final Cursor cursor = crud.ListarTodos();


        ArrayList<Aluno> listaArray = Aluno.lista();
        listView = findViewById(R.id.listaAlunos);
        dao = new AlunoDAO(this);
        alunos = dao.obterAlunos();

        ArrayAdapter<Aluno> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, alunos);
        //  listView.setAdapter(adapter);

        listView = (ListView) findViewById(R.id.listaAlunos);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String cod;

                cursor.moveToPosition(position);
                cod = cursor.getString(cursor.getColumnIndexOrThrow("id"));

                Intent intent = new Intent(ListarActivity.this, AtualizarActivity.class);
                intent.putExtra("cod", cod);
                startActivity(intent);
                finish();
            }
        });
        FloatingActionButton fbutton1 = (FloatingActionButton) findViewById(R.id.cad);
        fbutton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListarActivity.this, CadastrarActivity.class);
                startActivity(intent);
            }
        });



    }
}
