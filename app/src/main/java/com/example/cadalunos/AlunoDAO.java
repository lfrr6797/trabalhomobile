package com.example.cadalunos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class AlunoDAO {

    private Conexao conexao;
    private SQLiteDatabase banco;
    private Cursor cursor;

    public AlunoDAO(Context context) {
        conexao = new Conexao(context);
        banco = conexao.getWritableDatabase();

    }

    public long inserir(Aluno aluno) {
        ContentValues values = new ContentValues();
        values.put("nome", aluno.getNome());
        values.put("email", aluno.getEmail());
        values.put("telefone", aluno.getTelefone());
        values.put("idade", aluno.getIdade());
        return banco.insert("alunos", null, values);

    }



    public List<Aluno> obterAlunos() {
        List<Aluno> alunos = new ArrayList<>();
        Cursor cursor = banco.query("alunos", new String[]{"id", "nome", "email", "telefone", "idade"}, null, null, null, null, null);
        while (cursor.moveToNext()) {
            Aluno aluno = new Aluno();
            aluno.setId(cursor.getInt(0));
            aluno.setNome(cursor.getString(1));
            aluno.setEmail(cursor.getString(2));
            aluno.setTelefone(cursor.getString(3));
            aluno.setIdade(cursor.getString(4));
            alunos.add(aluno);
        }
        return alunos;
    }
    public Cursor ListarTodos(){
        banco = conexao.getReadableDatabase();
        cursor = banco.query("alunos",new String[]{"id", "nome", "email", "telefone", "idade"},null,null,null,null,null);
        if( cursor != null){
            cursor.moveToFirst();
        }

        return cursor;
    }

    public Cursor buscarID(int id){
        banco = conexao.getReadableDatabase();
        cursor = banco.query("alunos",new String[]{"id", "nome", "email", "telefone", "idade"},"id = "+id,null,null,null,null);

        if ( cursor != null){
            cursor.moveToFirst();
        }

        return cursor;
    }

    public void alterar (int id, String nome, String email, String telefone, String idade){
        ContentValues values = new ContentValues();
        long resultado;

        values.put("nome", nome);
        values.put("email", email);
        values.put("telefone", telefone);
        values.put("idade", idade);

        banco.update("alunos", values, "id = "+id, null);
    }

    public void deletar(int id){
        banco = conexao.getReadableDatabase();
        banco.delete("alunos", "id = "+ id, null);
    }



}
