package com.example.cadalunos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends LifeCycle {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final CheckBox checkBox = (CheckBox) findViewById(R.id.login);

        final EditText editTextUser = (EditText) findViewById(R.id.campoUsuario);
        editTextUser.setText(PreferenciasUsuario.getValuesString(getContext(),"nameUser"));

        if(!PreferenciasUsuario.getValuesBoolean(this.getContext(),"lembrarse")) {
            Button button = (Button) findViewById(R.id.buttonEntrar);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String user = editTextUser.getText().toString();

                    EditText editTextSenha = (EditText) findViewById(R.id.campoSenha);
                    String senha = editTextSenha.getText().toString();

                    if (user.equals("usuario") && senha.equals("1234")) {
                        Intent intent = new Intent(getContext(), ListarActivity.class);
                        Bundle params = new Bundle();
                        params.putString("nome", user);
                        intent.putExtras(params);

                        PreferenciasUsuario.setValuesString(getContext(), "nameUser", user);

                        if (checkBox.isChecked()) {
                            PreferenciasUsuario.setValuesBoolean(getContext(), "lembrarse", true);
                        } else {
                            PreferenciasUsuario.setValuesBoolean(getContext(), "lembrarse", false);
                        }

                        startActivity(intent);
                    } else {
                        alert("Falha ao efetuar login");
                    }

                }
            });
        }
        else {
            Intent intent = new Intent(getContext(), ListarActivity.class);
            startActivity(intent);
        }
    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public Context getContext() {
        return this;
    }

}
