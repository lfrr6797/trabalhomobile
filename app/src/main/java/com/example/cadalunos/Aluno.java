package com.example.cadalunos;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

public class Aluno implements Serializable {

    public static ArrayList<Aluno> alunos;

    private Integer id;
    private String nome,telefone,email,idade;

    public Aluno() {

        if(alunos == null){
            alunos = new ArrayList<>();
        }

    }

    public Aluno(String nome, String telefone, String email, String idade) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.idade = idade;
    }

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    public static  ArrayList<Aluno> lista(){
        return alunos;
    }


    @Override
    public String toString() {
        return "Nome: " + nome + "\nEmail: " + email + "\nTelefone: " + telefone + "\nIdade: " + idade + "\n";
    }
}
